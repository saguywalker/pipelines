### Thug pipeline  

Run a honeyclient (thug) on each URL in a file, get the analysis files in a separate commit

### How to set up the pipeline

1. Setup a git repository with a `master` branch with the URLs in a file `urls.txt` separated by newlines

2. Add your repository and SSH private key to the `creds.yml`

3. Login to concourse  

  ```fly -t $TARGET login -c https://concourse.cincan.io -u $USERNAME -p $PASSWORD```

4. Set up the pipeline  
 
  ```fly -t $TARGET set-pipeline -c pipeline.yml -p scan-urls -l creds.yml`

5. Unpause the pipeline  
 
  ```fly -t $TARGET unpause-pipeline -p scan-urls```

6. Trigger the job `run-thug`  

   ``fly -t $TARGET trigger-job -j scan-urls/run-thug```

---

#!/bin/sh

# Run static analysis tool binwalk over samples
# v. 0.1.

RESULTS=results-$(date).txt

set -e
set -x
set -u

ls results-2

cp -R results-2/* results/

ls results

# change location

cd results/binwalk

# run binwalk over samples

for f in ../../workspace-repo/samples/*; do
        echo "----------------------------------------------" >> $RESULTS
        echo "SAMPLE: "$f >> $RESULTS
        echo "DATE: "$(date) >> $RESULTS
        echo "----------------------------------------------" >> $RESULTS
        echo "" >> $RESULTS
	binwalk $f >> $RESULTS
done

# list files

ls -all

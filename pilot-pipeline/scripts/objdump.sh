#!/bin/sh

# Run static analysis tool objdump over samples
# v. 0.1.

RESULTS=results-$(date).txt

set -e
set -x
set -u

ls results

cp -R results/* results-2/

ls results-2

# change location

cd results-2/objdump

# run objdump over samples

for f in ../../workspace-repo/samples/*; do
        echo "----------------------------------------------" >> $RESULTS
        echo "SAMPLE: "$f >> $RESULTS
        echo "DATE: "$(date) >> $RESULTS
        echo "----------------------------------------------" >> $RESULTS
        echo "" >> $RESULTS
	objdump -f $f >> $RESULTS
done


# list files

ls -all

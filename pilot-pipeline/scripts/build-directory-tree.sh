#!/bin/sh

# Build directory tree for the results
# v. 0.1.

set -e
set -x
set -u

# show location

pwd

# create directory tree

mkdir results/strings/
mkdir results/binwalk/
mkdir results/objdump/
mkdir results/pdfid/
mkdir results/pdf-parser/
mkdir results/peepdf/
mkdir results/officemalscanner/
mkdir results/oledump/
mkdir results/ida/
mkdir results/radare2/
mkdir results/jad/
mkdir results/row-12/

ls results

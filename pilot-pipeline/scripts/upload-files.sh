#!/bin/sh

set -e
set -x

git clone workspace-repo updated-repo
cp -R results-2 updated-repo/results-2
cd updated-repo
git config --global user.email "joni@concourse"
git config --global user.name "joni"
git add .
git commit -m "update all results"

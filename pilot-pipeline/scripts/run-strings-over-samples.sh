#!/bin/sh

# This script assumes that there is a git checkout in the current directory under "workspace-repo".
# Inside, a directory called "samples/" contains the input data for the string extraction.

# This repository will be cloned to "updated-workspace-repo", with the extracted strings in the "found-strings.txt" file at the root of the repository.

set -e
set -x
set -u

git clone workspace-repo updated-workspace-repo
cd updated-workspace-repo
git config user.email "joni@concourse"
git config user.name "joni"
strings samples/hello_world > found-strings.txt
git add found-strings.txt
if [ -n "$(git status --porcelain)" ]; then
  git commit -a -m "extracted strings from samples"
else
  echo "no changes";
fi
ls /tmp/

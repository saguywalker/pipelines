import csv
import json
import sys

samples = sys.argv[1]
output_path = sys.argv[2]


with open(samples, "rb") as csvfile:
    reader = csv.reader(csvfile, delimiter=",")

    headings = next(reader)
    results = {}
    for row in reader:
        name = row[0].split('/')[1]
        level = row[3]
        if name in results and level in results[name]:
                results[name][level] += 1
        else:
            results[name] = {level: 1}

with open(output_path, "w") as outfile:
    json.dump(results, outfile, indent=2)
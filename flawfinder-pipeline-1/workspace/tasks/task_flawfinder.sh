#!/bin/sh
samples="input/samples/sources/*"
output_path="output/results/flawfinder.csv"
toolscript="input/tools/flawfinder/python-tools/flawfinder.py"

# Crash early
set -e

git clone input output

if [ ! -d "$output_path" ]; then
    mkdir $output_path
fi

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

ls $samples

flawfinder --csv samples>output.csv

python $toolscript output.csv output_path

cd output_path
ls results
git add results/*
git commit -m "dumped processes"

# Concourse Cortex-Abuse_Finder Pipeline

Concourse pipeline that uses Cortex tool Abuse_Finder to analyze data such as IP, email or url. 

## Quick Deployment

1. Set up [Concourse](https://concourse-ci.org/) ([useful tutorial](https://concoursetutorial.com/))
2. Copy the contents of server folder to your Concourse installation 
3. Create credentials.yml (example template in credentials_example.yml)
4. Setup the pipeline:
```
$ fly -t target-name sp -p pipeline-name -c pipeline.yml -l credentials.yml
```
5. Copy the contents of workspace_repo folder to repository you'll use for the pipeline
6. Put data samples to samples folder in your repository and trigger the pipeline

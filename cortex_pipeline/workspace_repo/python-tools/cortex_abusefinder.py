import os, sys, json, codecs, logging
from io import open
from abusefinder import AbuseFinderAnalyzer

if sys.version_info >= (3, 0):
    from io import StringIO
else:
    from StringIO import StringIO

def load_json(json_path):
    json_file = open(json_path)
    input = json_file.read()
    json_file.close()
    sys.stdin = StringIO(input)

def output_json(json_path, result):
    json_file=open(json_path, 'w')
    json_file.write(unicode(result))
    json_file.close()

def main():
    input_json = sys.argv[1]
    output = sys.argv[2]
    load_json(input_json)
    f = StringIO()
    sys.stdout = f
    AbuseFinderAnalyzer().run()
    output_json(output, f.getvalue())


if __name__ == '__main__':
    main()

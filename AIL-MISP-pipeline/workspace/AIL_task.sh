#!/bin/sh
samples="resource-repo/dump"
output="./resource-ail-tags/ail-dump"

git clone resource-repo resource-ail-tags

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

if [ ! -d "$output" ]; then
    mkdir $output
fi

python3 resource-repo/python-tools/submit_paste.py $samples/misp_data $output/misp_tool

cd resource-ail-tags
git add .
git commit -m "dumped data"


#!/bin/sh
output="./resource-repo/dump"

git clone resource-repo resource-fetched-sample

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

if [ ! -d "$output" ]; then
    mkdir $output
fi

python3 resource-repo/python-tools/MISP_tool.py misp_data

cd resource-repo
git add .
git commit -m "dumped samples"

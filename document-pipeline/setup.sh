#!/bin/bash
PIPELINE="document-pipeline"
CREDS="credentials.yml"
CHECK="\e[32m[+]\e[0m"
ERROR="\e[0;91m[x]\e[0m"
WARNING="\e[33m[!]\e[0m"


# 1. Print out public cincan_rsa, ask if it is set in the local Gitlab
echo -e $WARNING "1. Add this SSH key to gitlab first, otherwise the script will fail: \n"
cat /opt/cincan/keys/cincan_rsa.pub |awk '{print $1,$2}'
read -r -p "Is the SSH key set? [y/N] " Yn
[[ "$Yn" =~ ^([yY][eE][sS]|[yY])+$ ]] && echo -e $CHECK "Continuing installation" || exit 1



# 2. Modify credentials.yml with correct Gitlab repository and credentials
echo -e $CHECK "2. Modifying /opt/cincan/pipelines/credentials.yml"
sed -i -e "s#git@<GITLAB>:<USER>/<REPOSITORY>#git@172.20.0.5:root/$PIPELINE#" $CREDS

# Count lines before key
COUNT=$(awk '/private-key/{ print NR; exit }' $CREDS)

# Remove the rest of the lines
sed -i ""$((COUNT+1))","$(wc -l $CREDS |awk '{print $1}')"d" $CREDS

# Count indent of last line, create ssh key indent according to it
INDENT=$(awk '{ match($0, /^ */); printf("%d", RLENGTH) }' $CREDS | awk '{print substr($NF,'$COUNT')}')
for ((i=1;i<$((INDENT+3));i++)); do  SPACE+=" "; done

# Append creds with rsa
cat /opt/cincan/keys/cincan_rsa >> $CREDS

# Count lines, indent from line number $COUNT until line number $ENDLINE
ENDLINE=$(awk 'END{print NR}' $CREDS)
sed -i ""$((COUNT+1))","$ENDLINE"s/^/$(echo "$SPACE")/" $CREDS

# Copy pipeline files to /opt/cincan/pipelines
[ ! -d "pipelines" ] && mkdir /opt/cincan/pipelines
cp credentials.yml pipeline.yml /opt/cincan/pipelines/



# 3. Create repositories to the local gitlab using SSH. Key not added to known_hosts, but /dev/null
echo -e $CHECK "3. Creating pipeline repositories to the local Gitlab"
cd scripts
chmod +x *
git init
git add .
git config user.email "cincan@cincan.io"
git config user.name "cincan"
git commit -m "cincan pipeline test"
git remote add origin git@172.20.0.5:root/$PIPELINE.git
echo 'git config --local core.sshCommand "ssh -i /opt/cincan/keys/cincan_rsa -o UserKnownHostsFile=/dev/null"'
git config --local core.sshCommand "ssh -i /opt/cincan/keys/cincan_rsa -o UserKnownHostsFile=/dev/null"
git -c http.sslVerify=false push -u origin master || { echo -e $ERROR "Git failed, is the SSH key set?" ; exit 1; }

cd ../samples
git init
git add .
git config user.email "cincan@cincan.io"
git config user.name "cincan"
git commit -m "cincan pipeline test"
git remote add origin git@172.20.0.5:root/$PIPELINE.git
git checkout -b sample-source
echo 'git config --local core.sshCommand "ssh -i /opt/cincan/keys/cincan_rsa -o UserKnownHostsFile=/dev/null"'
git config --local core.sshCommand "ssh -i /opt/cincan/keys/cincan_rsa -o UserKnownHostsFile=/dev/null"
git -c http.sslVerify=false push -u origin sample-source || { echo -e $ERROR "Git failed, is the SSH key set?" ; exit 1; }

cd ../samples
git init
git add .
git config user.email "cincan@cincan.io"
git config user.name "cincan"
git commit -m "cincan pipeline test"
git remote add origin git@172.20.0.5:root/$PIPELINE.git
git checkout -b results
echo 'git config --local core.sshCommand "ssh -i /opt/cincan/keys/cincan_rsa -o UserKnownHostsFile=/dev/null"'
git config --local core.sshCommand "ssh -i /opt/cincan/keys/cincan_rsa -o UserKnownHostsFile=/dev/null"
git -c http.sslVerify=false push -u origin results || { echo -e $ERROR "Git failed, is the SSH key set?" ; exit 1; }


# Create instructions file
cat > /opt/cincan/build/PIPELINE-README << EOL
Login to concourse:
    docker exec -it concourse.cincan.io fly -t local login --ca-cert /opt/cincan/certs/concourse.cincan.io.crt -c https://172.20.0.3:443 -u <USERNAME> -p <PASSWORD>

Setup the pipeline:
    docker exec -it concourse.cincan.io fly -t local set-pipeline -p $PIPELINE -c /opt/cincan/pipelines/pipeline.yml -l /opt/cincan/pipelines/credentials.yml

Unpause the pipeline from command line:
    docker exec -it concourse.cincan.io fly -t local unpause-pipeline -p $PIPELINE

Destroy the pipeline:
    docker exec -it concourse.cincan.io fly -t local destroy-pipeline -p $PIPELINE
EOL



# 4. Run Concourse commands

# Login
echo -e $CHECK "4. Login to Concourse and setup the pipeline"
echo "Pipeline created."
read -r -p "Login in to concourse now? [y/N] " Yn
if [[ "$Yn" =~ ^([yY][eE][sS]|[yY])+$ ]];then
	# Prompt for username & password
	echo -n "username: "
	read HANDLE
	echo -n "password: "
	read -s PASS
	# Run concourse login
	docker exec -it concourse.cincan.io fly -t local login --ca-cert /opt/cincan/certs/concourse.cincan.io.crt -c https://172.20.0.3:443 -u $HANDLE -p $PASS || { echo -e $ERROR "Login failed, see PIPELINE-README" ; exit 1; }
else
	echo "For concourse commands see the file PIPELINE-README"; exit
fi

# Set up the pipeline
echo ""
read -r -p "Set up the pipeline now? [y/N] " Yn
if [[ "$Yn" =~ ^([yY][eE][sS]|[yY])+$ ]];then
    echo -e "docker exec -it concourse.cincan.io fly -t local set-pipeline -p $PIPELINE -c /opt/cincan/pipelines/pipeline.yml -l /opt/cincan/pipelines/credentials.yml"
    docker exec -it concourse.cincan.io fly -t local set-pipeline -p $PIPELINE -c /opt/cincan/pipelines/pipeline.yml -l /opt/cincan/pipelines/credentials.yml
else
    echo "For concourse commands see the file PIPELINE-README"; exit
fi

echo -e $CHECK "Pipeline set up"
echo "Unpause the pipeline:"
echo "docker exec -it concourse.cincan.io fly -t local unpause-pipeline -p $PIPELINE"
echo "  OR from the concourse ui:"
echo "  https://172.20.0.3"
echo ""
echo "For concourse commands see the PIPELINE-README."

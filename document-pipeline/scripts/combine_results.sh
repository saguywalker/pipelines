#not in use

#!/bin/sh
git clone results output-files
SAMPLESPATH=$(pwd)

# Combine logs from work, docs and results -branches --> results
cd $SAMPLESPATH/output-files
echo $(date) > $SAMPLESPATH/output-files/_timestamp_
rm -rf $SAMPLESPATH/output-files/sample
cp $SAMPLESPATH/work/temp/*.log $SAMPLESPATH/output-files/ 2>/dev/null

git add .
git config --global user.name "cincan-pipeline"
git config --global user.email "cincan@concourse"
git commit -m "update final results"
git pull

#!/bin/bash
SAMPLESPATH=$(pwd)
git clone results updated-results
git clone sample-source updated-source

ls -lR

cd $SAMPLESPATH/updated-source
if [[ "$(ls $SAMPLESPATH/updated-source |wc -l)" != 0 ]]; then
    # Remove old log
    rm $SAMPLESPATH/updated-results/filetypes.log
    find $SAMPLESPATH/updated-source/* -prune -type f | while IFS= read -r sample
        do
            #if [ ! -d "$sample" ]; then
                echo "Processing: " $sample
                file $sample --mime-type |awk -F'[/]' '{print $3}'
                echo File: ${sample##*/} | tee -a $SAMPLESPATH/updated-results/filetypes.log
                FILETYPE=$(file $sample --mime-type | awk '{print $2}' | cut -d "/" -f 2)
                # Make a log of files' types
                echo "Type: " $FILETYPE | tee -a $SAMPLESPATH/updated-results/filetypes.log
                if [[ $FILETYPE != 'pdf' ]]; then
                    echo "file: " $sample
                    if [ ! -d $SAMPLESPATH/updated-source/doc ]; then mkdir $SAMPLESPATH/updated-source/doc; fi
                    mv $sample $SAMPLESPATH/updated-source/doc/
               fi
            #fi
        done
        ls -l $SAMPLESPATH/updated-source/
else
    echo "No files to analyse"
fi

git config user.name "cincan"
git config user.email "cincan@cincan.io"
git add .
git commit -m "update source after sorting"
git pull
ls -l $SAMPLESPATH/updated-source/

cd $SAMPLESPATH/updated-results
git config user.name "cincan"
git config user.email "cincan@cincan.io"
git add .
git commit -m "update log of sorted files"
git pull
ls -l $SAMPLESPATH/updated-results/

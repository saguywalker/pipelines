#!/bin/sh
SAMPLESPATH=$(pwd)
ls $SAMPLESPATH/sample-source/ -R

number_of_files=$(ls $SAMPLESPATH/sample-source/doc |wc -l)

# Loop through samples, or if folder is empty, create "empty" log.
if [[ "$number_of_files" == 0 ]]; then
    echo "Folder is empty"
    echo $(date) > $SAMPLESPATH/output-files/oledump-empty.log
else
    echo "Processing files"
    file_number=1
    
    # Scan the files
    for file in $SAMPLESPATH/sample-source/doc/*
        do
            if [ ! -d $file ]; then
                echo -e "------------------------------------------------------------------\nAnalysing:" ${file##*/}
                hash=$(sha256sum "${file}" | cut -d ' ' -f1)
                echo -e ${file} "\n"sha256 $hash "\n" >> $SAMPLESPATH/sample-source/doc/oledump.log
                /usr/bin/python /oledump.py $file | tee -a $SAMPLESPATH/sample-source/doc/oledump.log

                # Scan through streams found in the file
                streams=$(/usr/bin/python /oledump.py $file |cut -f1 -d":" |sed 's/ //g' |grep [0-9])
                echo "Streams:" $streams | tee -a $SAMPLESPATH/sample-source/doc/oledump.log
                for s in $streams
                do
                    echo -e "Stream $s: \n" | tee -a $SAMPLESPATH/sample-source/doc/oledump.log
                    /usr/bin/python /oledump.py $file -s $s| tee -a $SAMPLESPATH/sample-source/doc/oledump.log
                done
            fi
        done

    cp $SAMPLESPATH/sample-source/doc/*.log $SAMPLESPATH/output-files/ 2>/dev/null
fi
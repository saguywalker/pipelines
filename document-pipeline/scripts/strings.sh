# Run Strings to all but PDF files
#!/bin/sh
SAMPLESPATH=$(pwd)
ls $SAMPLESPATH/sample-source/ -R

# Do nothing if folder is empty, otherwise loop through samples.
number_of_files=$(ls $SAMPLESPATH/sample-source/doc |wc -l)
if [[ "$number_of_files" == 0 ]]; then
	echo "Folder is empty"
    echo $(date) > $SAMPLESPATH/output-files/strings-empty.log
else
	# Scan the files
	echo "Processing files"
	file_number=1
	for file in $SAMPLESPATH/sample-source/doc/*
		do
	        if [ ! -d $file ]; then
	            hash=$(sha256sum "${file}" | cut -d ' ' -f1)
    	        echo "-------------------------------------" | tee -a $SAMPLESPATH/output-files/strings.log
    	        echo File: ${file##*/} $hash | tee -a $SAMPLESPATH/output-files/strings.log
    	        strings $file | tee -a $SAMPLESPATH/output-files/strings.log
    	    fi
        done
fi

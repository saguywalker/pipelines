#!/bin/sh
SAMPLESPATH=$(pwd)
ls $SAMPLESPATH/sample-source/ -R

# Do nothing if folder is empty, otherwise loop through samples.
number_of_files=$(ls $SAMPLESPATH/sample-source/doc |wc -l)
if [[ "$number_of_files" == 0 ]]; then
    echo "Folder is empty"
    echo $(date) > $SAMPLESPATH/output-files/olevba-empty.log
else
        # Scan the files
        echo "Processing files"
        file_number=1
        for file in $SAMPLESPATH/sample-source/doc/*
                do
                if [ ! -d $file ]; then
                echo Analysing: ${file##*/}
                hash=$(sha256sum "${file}" | cut -d ' ' -f1)
                echo -e ${file} "\n"sha256 $hash "\n" >> $SAMPLESPATH/sample-source/doc/olevba.log
                olevba $file -a | tee -a $SAMPLESPATH/sample-source/doc/olevba.log 
            fi
        done
    
    cp $SAMPLESPATH/sample-source/doc/*.log $SAMPLESPATH/output-files/ 2>/dev/null
fi

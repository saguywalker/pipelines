#!/bin/bash
SAMPLESPATH=$(pwd)
ls $SAMPLESPATH/sample-source/ -R

# Do nothing if folder is empty, otherwise loop through samples.
number_of_files=$(ls -p sample-source/ | grep -Ev '/' |wc -l)
if [[ "$number_of_files" == 0 ]]; then
	echo "Folder is empty"
	echo $(date) | tee -a $SAMPLESPATH/output-files/pdfid-empty.log
else
	# Scan the files
	echo "-------------------------------------------------"
	echo "Processing " $number_of_files "files"...
	cd /pdfid
	file_number=1
	ls $SAMPLESPATH/sample-source/
	for file in $SAMPLESPATH/sample-source/*
		do
	        if [ ! -d $file ]; then
    	        echo Analysing: ${file##*/}
	            hash=$(sha256sum "${file}" | cut -d ' ' -f1)
	            /usr/bin/python pdfid.py $file -p plugin_triage sample-source/*.* > result
    	        if (cat result | tee -a $SAMPLESPATH/pdfid.log | grep -c 1.00);then
	                    echo $hash --- ${file##*/} >> $SAMPLESPATH/pdfid-malicious.log
	                    echo "Likely malicious"
	            elif (cat result | grep -c 0.00);then
	                    echo $hash --- ${file##*/} >> $SAMPLESPATH/pdfid-clean.log
	                    echo "Likely clean"
	            else
	                    echo $hash --- ${file##*/} >> $SAMPLESPATH/pdfid-requires-more-analysis.log
	                    echo "Needs further analysis"
	            fi
	            echo -e "\n$file_number" / $number_of_files;let "file_number=file_number+1"
	            echo --------------------------
	        fi
        done

    # Create logs
    echo "Results" | tee -a $SAMPLESPATH/pdfid.log
    echo "Total number of samples: $number_of_files" | tee -a $SAMPLESPATH/pdfid.log
    if [ -f $SAMPLESPATH/pdfid-malicious.log ]; then echo "Malicious: "$(wc -l $SAMPLESPATH/pdfid-malicious.log| cut -d ' ' -f1) | tee -a $SAMPLESPATH/pdfid.log ;fi
    if [ -f $SAMPLESPATH/pdfid-clean.log ]; then echo "Clean: "$(wc -l $SAMPLESPATH/pdfid-clean.log| cut -d ' ' -f1) | tee -a $SAMPLESPATH/pdfid.log ;fi
    if [ -f $SAMPLESPATH/pdfid-requires-more-analysis.log ]; then echo "Requires further analysis: "$(wc -l $SAMPLESPATH/pdfid-requires-more-analysis.log| cut -d ' ' -f1) | tee -a $SAMPLESPATH/pdfid.log ;fi
    
    # Copy created files to output
    cp $SAMPLESPATH/*.log $SAMPLESPATH/output-files/
fi

ls -R $SAMPLESPATH/output-files/
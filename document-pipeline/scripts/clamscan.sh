# Scan all files with ClamAV
#!/bin/sh
SAMPLESPATH=$(pwd)
ls $SAMPLESPATH/sample-source/ -R

# Do nothing if folder is empty, otherwise loop through samples.
number_of_files=$(ls $SAMPLESPATH/sample-source |wc -l)
if [[ "$number_of_files" == 0 ]]; then
	echo "Folder is empty"
    echo $(date) > $SAMPLESPATH/output-files/clamscan-empty.log
else
	# Scan the files
	echo "Processing files"
	
    /usr/bin/clamscan -r $SAMPLESPATH/sample-source/* --recursive | \
        tee -a $SAMPLESPATH/output-files/clamscan.log
    
    if [ ! -s $(cat $SAMPLESPATH/output-files/clamscan.log | grep FOUND ) ]; then \
        cat $SAMPLESPATH/output-files/clamscan.log | grep FOUND > $SAMPLESPATH/output-files/clamscan-infected.log; fi
fi

#!/bin/sh
samples="resource-repo/samples/*"
output="resource-manalyze-analysis/results"

set -e

git clone resource-repo resource-manalyze-analysis

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

if [ ! -d "$output" ]; then
    mkdir $output
fi

for filename in $samples
do
    directory=$(basename ${filename%.*})
    /Manalyze/bin/manalyze $filename --plugins=all --dump all -o json >> $output/$directory
done

cd resource-manalyze-analysis
git add results/*
git commit -m "dumped data"

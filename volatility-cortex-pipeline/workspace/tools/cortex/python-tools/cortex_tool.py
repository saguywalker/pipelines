import os, sys, json, codecs, logging
from io import open

from virustotal import VirusTotalAnalyzer

if sys.version_info >= (3, 0):
    from io import StringIO
else:
    from StringIO import StringIO

def parseVolatility(json_path):
    with open(json_path) as data_file:
        data = json.load(data_file)
    md5 = data['hashes'][0]['md5']
    api_key = "" #FIXME insert API key here
    template = '''{
  "analyzer": "VirusTotal",
  "data": "'''+md5+'''",
  "dataType": "hash",
  "tlp":0,
  "config": {
    "service": "get",
    "key": "'''+api_key+'''"
  }
}
'''
    sys.stdin = StringIO(template)

def output_json(json_path, result):
    json_file=open(json_path, 'w')
    json_file.write(unicode(result))
    json_file.close()

def main():
    input_json = sys.argv[1]
    output = sys.argv[2]
    f = StringIO()
    old = sys.stdout
    sys.stdout = f
    parseVolatility(input_json)
    VirusTotalAnalyzer().run()
    output_json(output, f.getvalue())
    sys.stdout = old
    print("file output succesful!")
    print("output: " + f.getvalue())


if __name__ == '__main__':
    main()

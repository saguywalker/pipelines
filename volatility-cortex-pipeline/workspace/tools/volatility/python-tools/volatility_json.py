#!/usr/bin/env python2.7

import sys
import os
import json

from plugins import *

def main():
    """Main function of the script
    """

    sample = sys.argv[1]
    output_path = sys.argv[2]

    output = {}

    output["memory profile"] = kdbgscan(sample)[0]
    output["processes"], output["hidden processes"] = hidden_processes(sample,
                                                                       output["memory profile"])
    output["connections"] = connections(sample, output["memory profile"])

    for process in output["hidden processes"]:
        dump_process(sample, output_path, process[0], process[1], output["memory profile"])

        output["dll"] = dll(sample, output["memory profile"], process[0], process[1])
        output["process memory"] = process_memory(sample, output["memory profile"], process[0], process[1])


    output["hashes"] = []
    for filename in os.listdir(output_path + "/executables"):
        if filename.endswith(".exe"):
            output["hashes"].append(calculate_hash(output_path + "/executables", filename))

    with open(output_path + "/volatility.json", "w") as outfile:
        print("Saving output to {}/volatility.json").format(output_path)
        json.dump(output, outfile, indent=2)


if __name__ == "__main__":
    main()

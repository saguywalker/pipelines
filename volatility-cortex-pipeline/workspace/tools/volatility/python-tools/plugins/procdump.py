from util import execute_dump

def dump_process(sample, output_path, offset, pid, profile):
    """Dumps a given process to an executable

    Keyword arguments:
    sample: path to memory sample
    output_path: path for output
    offset: memory offset of the process
    pid: process ID of the process
    profile: memory profile of the sample

    :return:
    """
    print "Dumping processes..."
    params = ["procdump",
             "-D", output_path + "/executables",
             "-f", sample,
             "-p", str(pid),
             "--offset=" + str(offset),
             "--profile=" + profile
            ]

    execute_dump(params)
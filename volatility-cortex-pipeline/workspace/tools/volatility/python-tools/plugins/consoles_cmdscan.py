from util import execute_plugin

def consoles(sample, profile):
    """Execute Volatility's cmdscan and consoles plugins

    sample:
    profile:
    :return:
    """

    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    output["consoles"] = execute_plugin(params, ["consoles"])
    output["cmdscan"] = execute_plugin(params, ["cmdscan"])

    return output
from util import execute_plugin

def process_memory(sample, profile, offset=None, pid=None):


    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    plugins = ["memmap", "vadinfo", "vadwalk", "vadtree", "memdump", "vaddump", "iehistory"]

    evt_profile = ["WinXPSP1x64",
                   "WinXPSP2x64",
                   "WinXPSP2x86",
                   "WinXPSP3x86",
                   "Win2003SP0x86",
                   "Win2003SP1x64",
                   "Win2003SP1x86",
                   "Win2003SP2x64",
                   "Win2003SP2x86"
                   ]

    if profile in evt_profile:
        execute_plugin(params, ["evtlogs"])

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    output["process memory"] = execute_plugin(params, plugins)

    return output

def vad(sample, profile, offset=None, pid=None):

    params = ["-f", sample,
              "--profile=" + profile
              ]
    output = {}

    plugins = ["vadinfo", "vadwalk", "vadtree", "vaddump"]

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    output["vad"] = execute_plugin(params, plugins)

    return output
import subprocess

"""
**************************************************
Instantiating KDBG using: Kernel AS WinXPSP3x86 (5.1.0 32bit)
Offset (V)                    : 0x80545b60
Offset (P)                    : 0x545b60
KDBG owner tag check          : True
Profile suggestion (KDBGHeader): WinXPSP3x86
Version64                     : 0x80545b38 (Major: 15, Minor: 2600)
Service Pack (CmNtCSDVersion) : 3
Build string (NtBuildLab)     : 2600.xpsp_sp3_gdr.080814-1236
PsActiveProcessHead           : 0x8055a1d8 (19 processes)
PsLoadedModuleList            : 0x80554040 (99 modules)
KernelBase                    : 0x804d7000 (Matches MZ: True)
Major (OptionalHeader)        : 5
Minor (OptionalHeader)        : 1
KPCR                          : 0xffdff000 (CPU 0)

"""

def kdbgscan(sample):
    """Execute Volatility's kdbgscan plugin that returns a memory profile suggestion for given
    memory sample

    Keyword arguments:
    sample: path to memory sample

    :return: List of suggested profiles
    """
    print "Running kdbgscan..."
    result = subprocess.check_output(["vol.py", "kdbgscan", "-f", sample], universal_newlines=True)
    result = result.split("\n")
    suggested_profiles = []
    result.pop()
    print len(result)
    for i in result:
        print i
    if len(result) % 16 == 0:
        profiles = [result[x : x + 16] for x in range(0, len(result),16)]
    else:
        return None

    for i in profiles:
        processes = i[9].split("(")[1].split()[0]
        modules = i[10].split("(")[1].split()[0]
        if processes != "0" and modules != "0":
            print i[5].split(":")[1]
            suggested_profiles.append(i[5].split(":")[1].strip())

    return suggested_profiles
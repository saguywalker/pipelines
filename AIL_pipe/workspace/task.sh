#!/bin/sh
samples="resource-repo/samples/*"
output="./resource-dumped-processes/dump"

set -e

git clone resource-repo resource-dumped-processes

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

for filename in $samples
do
    echo $filename
    directory=$(basename ${filename%.*})
    #if [ ! -d "$output/$directory" ]; then
        #mkdir $output/$directory
    #fi
    python3 resource-repo/python-tools/submit_paste.py $filename $output/$directory
done

cd resource-dumped-processes
git add dump/*
git commit -m "dumped data"


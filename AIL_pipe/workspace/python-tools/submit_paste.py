#!/usr/bin/env python3
# -*-coding:UTF-8 -*

'''
submit your own pastes in AIL

empty values must be initialized
'''


import requests, re, sys, os
import urllib.parse as urlparse
import time
from io import StringIO

def output_file(filename, data):
    json_file=open(filename, 'w')
    json_file.write(data)
    json_file.close()

def main():
    input_filename = sys.argv[1]
    output_filename = sys.argv[2]
    data_file = open(input_filename)
    data = data_file.read()
    data_file.close()

    ail_auth = #FIXME add username and password e.g. ("username", "password123")
    #AIL url
    base_url = #FIXME add AIL url e.g.: 'https://ci2.cincan.virtues.fi/ail/'

    ail_url = base_url + 'PasteSubmit/submit'

    # MIPS TAXONOMIE, need to be initialized (tags_taxonomies = '')
    # tags_taxonomies = 'CERT-XLM:malicious-code=\"ransomware\",CERT-XLM:conformity=\"standard\"'
    tags_taxonomies = 'infoleak:source=\"automatic-collection\"'

    # MISP GALAXY, need to be initialized (tags_galaxies = '')
    tags_galaxies = 'misp-galaxy:backdoor="WellMess"'

    # user paste input, need to be initialized (paste_content = '')
    paste_content = 'paste content test'

    #file full or relative path
    file_to_submit = 'test_file.zip'

    #compress file password, need to be initialized (password = '')
    password = ''

    '''
    submit user text
    '''
    print("p1", ail_url)
    r = requests.post(ail_url, data={   'password': password,
                                        'paste_content': paste_content,
                                        'tags_taxonomies': tags_taxonomies,
                                        'tags_galaxies': tags_galaxies}
                               , auth=ail_auth, allow_redirects=False
                               )
    print(r.status_code, r.reason)
    if r.status_code >= 400:
        print("error details:", r.text)
    else:
        m = re.search(r"'(/PasteSubmit/submit_status.*?)'", r.text)
        if m is None:
            print("PasteSubmit status url not found, text:", r.text, "location", r.headers.get("location"))
        else:
            status_url = urlparse.urljoin(ail_url, '/ail' + m.group(1))
            paste_url = None
            print(status_url)
            while True:
                print("g1", status_url)
                sr = requests.get(status_url, auth=ail_auth)
                print(sr.text)
                status = sr.json()
                if status["end"]:
                    paste_url = re.search('"/(showsavedpaste/.*?)"', status["link"]).group(1)
                    break
                else:
                    time.sleep(1)
            final_paste_url = urlparse.urljoin(base_url, paste_url)
            time.sleep(5)
            fsr = requests.get(final_paste_url, auth=ail_auth)
            lines = [l.strip().replace('&#34;', "'") for l in fsr.text.splitlines() if re.search(r'\bbtn-(success|primary)\b', l)]
            lines = [re.sub(r'<span[^>]*>', '', l) for l in lines]
            lines = [re.sub(r'</span>.*', '', l) for l in lines if not re.match(r'^<(a href|button)', l)]
 
            f = StringIO()
            old_stdout = sys.stdout
            sys.stdout = f
            print(lines)
            out_data = f.getvalue()
            sys.stdout = old_stdout

            output_file(output_filename, out_data)
            
if __name__ == '__main__':
    main()





# first run, to install os:
# 1. create image file
#    example: qemu-img create -f qcow2 ~/vm/concvm1.img 10G
# 2. invoke this script with the iso path as first argument
#    example: bash concvm.sh ~/Downloads/ubuntu-18.04-live-server-amd64.iso
# 3. install concourse quickstart, either using docker-compose or using the "concourse quickstart" command as coverd in the Concourse docs. Or using the ansible recipe at https://github.com/ahelal/ansible-concourse

# by default host->vm port forwards are set up as follows:
#   8080: concourse HTTP
#   8022: ssh
#
# to add port forwards on the fly:
# 1. go to qemu monitor (hold down alt + ctrl + shift + 2)
# 2. type in something like hostfwd_add tcp:127.0.0.1:8022-:22

isofile=$1
if [ -f "$isofile" ]; then
    echo "Using $isofile as cdrom image"
    cdromarg="--boot once=d --drive media=cdrom,file=$isofile,readonly"
else
    cdromarg="--boot once=c"
fi
listenaddr=127.0.0.1
kvm -smp 2 -m 1500 \
    -netdev user,id=mynet0,hostfwd=tcp:$listenaddr:9022-:22,hostfwd=tcp:$listenaddr:9080-:8080 \
    --device virtio-net-pci,netdev=mynet0 -vga qxl \
    --boot once=d \
    $cdromarg \
    --drive file=$HOME/vm/concvm1.img,format=qcow2 
   

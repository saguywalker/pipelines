#!/usr/bin/env python2.7

import sys
import os
import json
from datetime import datetime
import plugins

def main():
    """Main function of the script, currently very ad hoc
    """

    sample = sys.argv[1]
    output_path = sys.argv[2]
    output = {}

    try:
        output["memory profile"] = plugins.kdbgscan.kdbgscan(sample)[0]
    except TypeError:
        return

    output["modified"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    output["processes"], output["hidden processes"] = plugins.psxview.psxview(sample,
                                                                       output["memory profile"])

    if output["memory profile"] in plugins.connections.get_profiles():
        # only run these plugins if the sample is from supported OS
        output["connections"] = plugins.connections.connections(sample, output["memory profile"])

    for process in output["hidden processes"]:
        plugins.procdump.procdump(sample, output_path, output["memory profile"], process[0], process[1])

    output["hashes"] = []
    for filename in os.listdir(output_path + "/executables"):
        if filename.endswith(".exe"):
             output["hashes"].append(plugins.util.file_hash(output_path + "/executables", filename))

    for process in output["processes"]:
        output["certs"] = plugins.dumpcerts.dumpcerts(sample, output["memory profile"], process[0], process[1])

    output_name = plugins.util.dict_hash(output)
    with open("{}/{}.json".format(output_path, output_name), "w") as outfile:
        print("Saving output to {}/{}.json").format(output_path, output_name)
        json.dump(output, outfile, sort_keys=True, ensure_ascii=True, separators=(', ', ': '), indent=2)


if __name__ == "__main__":
    main()

import util


def procdump(sample, profile, output_path, offset, pid):
    """Dumps a given process to an executable

    Keyword arguments:
    sample: path to memory sample
    profile: OS memory profile of the sample
    output_path: path for output
    offset: memory offset of the process
    pid: process ID of the process

    :return:
    """
    print "Dumping processes..."
    params = ["procdump",
             "-D", output_path + "/executables",
             "-f", sample,
             "-p", str(pid),
             "--offset=" + str(offset),
             "--profile=" + profile
            ]

    util.execute_dump(params)

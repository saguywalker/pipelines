import util

def dlldump(sample, profile, output_path=None, offset=None, pid=None):
    """Execute Volatility's dlllist and dlldump (if output_path is set) plugins

    Keyword arguments:
    sample: path to memory sample
    output_path: path for output
    offset: memory offset of the process
    pid: process ID of the process
    profile: OS memory profile of the sample

    :return: List of suggested profiles
    """

    print "Running dlllist and dlldump..."
    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    output["dlllist"] = util.execute_plugin(params, ["dlllist"])

    if output_path:
        params += ["-D", output_path]
        util.execute_dump(params)

    return output
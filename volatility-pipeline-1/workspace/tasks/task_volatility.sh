#!/bin/sh
samples="resource-samples/samples/*"
output="./resource-dumped-processes/results"
toolscript="resource-samples/tools/volatility/python-tools/volatility_json.py"

# Crash early
set -e

git clone resource-samples resource-dumped-processes

# Create output folder if necessary
if [ ! -d "$output" ]; then
    mkdir $output
fi

git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

# Loop through sample files, create output folders if necessary
for filename in $samples
do
    directory="$(sha256sum $filename|cut -d " " -f 1)"
    if [ ! -d "$output/$directory" ]; then
        mkdir $output/$directory
    fi
    python $toolscript $filename $output/$directory
done

cd resource-dumped-processes
git add results/*
git commit -m "dumped processes"

#!/bin/sh
samples="resource-samples/samples/*"
tools="resource-tools/"
txt="resource-txt/"
other="resource-other/

set -e



for filename in $samples
do
    if [[ $(file --mime-type -b "$filename") == text/plain ]]; then
        cp $filename $txt
    else
        cp $filename $other
    fi
done

cd resource-dumped-processes
git add dump/*
git commit -m "dumped processes"


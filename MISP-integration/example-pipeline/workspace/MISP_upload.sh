#!/bin/sh
set -e

git config --global user.email "cincan@cincan.io"
git config --global user.name "cincan"

sed -i "s/True/False/g" /workfolder/keys.py
sed -i "s/<your MISP URL>/$mispaddress/g" /workfolder/keys.py
sed -i "s/Your MISP auth key/$mispkey/g" /workfolder/keys.py

python /workfolder/add_named_attribute.py -e $eventID -t other -v "`cat resource-repo/results/*`"
echo "Done"


#!/bin/sh
samples="resource-repo/samples/*"
output="resource-sample-analysis/results"

set -e

git clone resource-repo resource-sample-analysis

git config --global user.email "cincan@cincan.io"
git config --global user.name "cincan"

if [ ! -d "$output" ]; then
    mkdir $output
fi

for filename in $samples
do
    directory=$(basename ${filename%.*})
    /Manalyze/bin/manalyze $filename --plugins=all --dump all -o json >> $output/$directory
done

cd resource-sample-analysis
git add results/*
git commit -m "analyzed data"

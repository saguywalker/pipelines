WORK-IN-PROGRESS

Example script that uses MISP zmq to listen for events with relevant attachments that could be further analysed with some CinCan pipeline.

Usage: 
Edit keys.py and insert your misp URL and auth key.

Run cincan_zmq.py on the environment you're running MISP in with the following command (or add relevant values with -r and -p):

python cincan_zmq.py --host <MISP URL> --only misp_json

Note that the script assumes the location of the document-pipeline repo is /opt/cincan/build/pipelines/document-pipeline/samples. If this is not correct, edit script cincan_zmq.py variable "repository" with the correct local absolute path of the repo.
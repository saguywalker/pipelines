#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# General Public License v3.0


import argparse, sys, zmq, json, time, pprint, re, argparse, subprocess
from pymisp import PyMISP
from keys import misp_url, misp_key, misp_verifycert
from subprocess import Popen
from git import Repo

def init(url, key):
    return PyMISP(url, key, misp_verifycert, 'json')

def fetch_misp_attachment(filename, attribute_id):
    misp = init(misp_url, misp_key)

    with open(filename, 'wb') as f:
        out = misp.get_attachment(attribute_id)
        if isinstance(out, dict):
            print(out)
        else:
            f.write(out)

def upload_to_git(repo, file):

    repo = Repo(repo)
    assert not repo.bare
    repo.index.add([file])
    repo.index.commit("%s uploaded for analysis" % (file))
    origin = repo.remote(name='origin')
    origin.pull
    origin.push()


pp = pprint.PrettyPrinter(indent=4, stream=sys.stderr)

parser = argparse.ArgumentParser(description='Generic ZMQ client to gather events, attributes and sighting updates from a MISP instance')
parser.add_argument("-s","--stats", default=False, action='store_true', help='print regular statistics on stderr')
parser.add_argument("-p","--port", default="50000", help='set TCP port of the MISP ZMQ (default: 50000)')
parser.add_argument("-r","--host", default="127.0.0.1", help='set host of the MISP ZMQ (default: 127.0.0.1)')
parser.add_argument("-o","--only", action="append", default=None, help="set filter (misp_json, misp_json_event, misp_json_attribute or misp_json_sighting) to limit the output a specific type (default: no filter)")
parser.add_argument("-t","--sleep", default=0.1, help='sleep time (default: 0.1)', type=int)
args = parser.parse_args()

if args.only is not None:
        filters = []
        for v in args.only:
                filters.append(v)
        sys.stderr.write("Following filters applied: {}\n".format(filters))
        sys.stderr.flush()

port = args.port
host = args.host
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect ("tcp://%s:%s" % (host, port))
socket.setsockopt(zmq.SUBSCRIBE, b'')

poller = zmq.Poller()
poller.register(socket, zmq.POLLIN)

if args.stats:
    stats = dict()

while True:
    socks = dict(poller.poll(timeout=None))
    if socket in socks and socks[socket] == zmq.POLLIN:
            message = socket.recv()
            topic, s, m = message.decode('utf-8').partition(" ")
            if args.only:
                if topic not in filters:
                        continue

            json_m = json.loads(m)
            try:
                for i in json_m['Event']['Attribute']:
                    if i.get('type') == 'attachment' and i.get('value') != None:
                        attr = i.get('id')
                        repository = "/opt/cincan/build/pipelines/document-pipeline/samples" #absolute path to local repository folder of the pipeline (with the .git file)
                        filename=repository + i.get('value')
                        event = i.get('event_id')
                        print("Relevant attachment found for enrichment! Attribute:", attr)
                        print("EventID:", event)
                        print("Fetching sample and uploading it to git for analysis.")
                        fetch_misp_attachment(filename,attr)
                        upload_to_git(repository, i.get('value'))
                        print("Done!")

            except ValueError:
                continue

            if args.stats:
                stats[topic] = stats.get(topic, 0) + 1
                pp.pprint(stats)
            time.sleep(args.sleep)


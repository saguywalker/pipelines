1. Set up Concourse. You can follow [our](https://gitlab.com/CinCan/Tools/wikis/Getting-Started/2-Setting-up-the-pipeline) tutorial on how to do it.
2. Set up Cuckoo. You can follow our [Cuckoo-quickstart](https://gitlab.com/CinCan/Tools/wikis/Getting-Started/A.4-cuckoo-sandbox-quickstart)guide on how to do it.
3. Start Cuckoo rest API with command: 
```
cuckoo api --host 172.17.0.1
```
4. Copy the workspace folder to your Git setup.
5. Add the address and ssh credentials of your workspace repository to credentials.yml.
6. Setup the pipeline with fly.
```
fly -t target-name set-pipeline -p pipeline-name -c cuckoo-pipe.yml -l credentials.yml
```
7. Upload any file you wish to be analyzed with cuckoo under samples folder and trigger the pipeline.

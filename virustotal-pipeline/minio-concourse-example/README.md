## Example concourse-minio combination for suricata-virustotal pipeline

### Quick installation
1. `mkdir data` to add minio mounting point folder
2. Run generate-keys.sh
3. `docker-compose up -d`

### Notes
Default credentials for concourse are:   
```
username = test   
password = test   
```
Minio will run on :9000 port with credentials:   
```
username = minio   
password = minio123   
```
